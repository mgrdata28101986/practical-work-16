﻿#include <iostream>
#include <time.h>

using std::cout;
using std::endl;

int getCurrentDay()
{
    time_t currentTime = time(NULL);

    struct tm ptm;

    localtime_s(&ptm, &currentTime);

    return ptm.tm_mday;
}

int main()
{
    setlocale(0, "");

    const int MAX_NVALUE = 8;

    int myArray [MAX_NVALUE][MAX_NVALUE];

    for (int i = 0; i < MAX_NVALUE; i++)
    {
        for (int j = 0; j < MAX_NVALUE; j++)
        {
            myArray[i][j] = i + j;

            cout << myArray[i][j] << "\t";
        }
        cout << endl;
    }
    
    int calcArrayIdx = getCurrentDay() % MAX_NVALUE;

    int calcResult = 0;

    for (int j = 0; j < MAX_NVALUE; j++)
    {
        calcResult += myArray[calcArrayIdx][j];
    }

    cout << "\nСумма элементов строки #" << calcArrayIdx << ": " << calcResult << endl;

    return 0;
}
